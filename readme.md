# Native OneWire access for NodeJS over I2C and DS2482 with multiple master support.

## Forked from:

https://github.com/tg-x/W1Direct
https://github.com/jjoschyy/W1Direct (original author)

## Node API

Known to work with Node 13.7.0.