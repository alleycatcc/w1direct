#ifndef UPDATE_H
#define UPDATE_H

#include "api.h"
#include <node.h>

using namespace v8;


class Update: public Api   {

public:
  static void DeviceById(const FunctionCallbackInfo<Value>&);

private:
  static bool AssertUpdaterExists(const FunctionCallbackInfo<Value>&);
  static bool AssertUpdaterValue(const FunctionCallbackInfo<Value>&);

  static std::string GetUpdaterName(const FunctionCallbackInfo<Value>&);
  static bool ExecuteUpdater(const FunctionCallbackInfo<Value>&);

};


#endif
