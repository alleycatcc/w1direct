#ifndef READ_H
#define READ_H

#include "api.h"
#include <node.h>

using namespace v8;


class Read: public Api   {

public:
  static void DevicesById(const FunctionCallbackInfo<Value>&);


private:
  static int GetFieldBitMask(const FunctionCallbackInfo<Value>&);


};


#endif
