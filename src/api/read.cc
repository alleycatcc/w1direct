#include "read.h"
#include "../shared/util.h"
#include <vector>
#include <algorithm>

using namespace v8;

#define CP_FIELDS "fields"
#define CV_FIELDS "values|properties|connection"


void Read::DevicesById(const FunctionCallbackInfo<Value>& args) {
	bool validArgs =
	  AssertParamsFormat(args) 				  	 	  &&
  	  AssertDefaultParam(args, DP_DEVICE_IDS) 	 	  &&
  	  AssertParam(args, CP_FIELDS, DT_ARRAY)   	 	  &&
  	  AssertArrayParamIn(args, CP_FIELDS, CV_FIELDS)  &&
	  AssertDevices(args);

    if (!validArgs) {
        Isolate *isolate = Isolate::GetCurrent();
        args.GetReturnValue().Set(Undefined(isolate));
        return;
    }
    std::vector<Device*> devices = GetDevices(args);
    Local<Object> result = DevicesToV8Object(&devices, GetFieldBitMask(args));
    args.GetReturnValue().Set(result);
}


//private

int Read::GetFieldBitMask(const FunctionCallbackInfo<Value>& args){
    Isolate *isolate = Isolate::GetCurrent();
    Local<Context> context = isolate->GetCurrentContext();

	int mask=0; std::string field;
	Local<Array> fields = GetV8ArrayParam(args, CP_FIELDS);

	for (unsigned int i=0; i<fields->Length(); ++i){
		field = V8ValueToStdString(fields->Get(context, i).ToLocalChecked());

		Util::AddToBitMaskIf(&mask, &field, "values", 	  DDT_VALUES);
		Util::AddToBitMaskIf(&mask, &field, "properties", DDT_PROPERTIES);
		Util::AddToBitMaskIf(&mask, &field, "connection", DDT_CONNECTION);
	}

	return mask;
}


