#include "sync.h"
#include "../controller/controller.h"
#include <node.h>

using namespace v8;


void Sync::AllDevices(const FunctionCallbackInfo<Value>& args) {
  Controller* ctl = GetController(args);
  DeviceStore* ds = ctl->GetDeviceStore();
  ds->ResetAllChanges(true);
  ctl->SyncAllDevices();
  args.GetReturnValue().Set(ResponseSummary(ds));
}



void Sync::MasterDevices(const FunctionCallbackInfo<Value>& args) {
  Controller* ctl = GetController(args);
  DeviceStore* ds = ctl->GetDeviceStore();

  bool validArgs =
	AssertParamsFormat(args) &&
	AssertDefaultParam(args, DP_MASTER_NAME) &&
	AssertMaster(args);

  if (!validArgs) {
      Isolate *isolate = Isolate::GetCurrent();
      args.GetReturnValue().Set(Undefined(isolate));
      return;
  }
  ds->ResetAllChanges(true);
  ctl->SyncMasterDevices(GetMaster(args));
  args.GetReturnValue().Set(ResponseSummary(ds));
}



void Sync::BusDevices(const FunctionCallbackInfo<Value>& args) {
  Controller* ctl = GetController(args);
  DeviceStore* ds = ctl->GetDeviceStore();

  bool validArgs =
	AssertParamsFormat(args) &&
    AssertDefaultParam(args, DP_MASTER_NAME) &&
    AssertDefaultParam(args, DP_BUS_NUMBER)  &&
	AssertMaster(args) &&
	AssertBus(args);

  if (!validArgs) {
      Isolate *isolate = Isolate::GetCurrent();
      args.GetReturnValue().Set(Undefined(isolate));
      return;
  }
  ds->ResetAllChanges(true);
  ctl->SyncBusDevices(GetBus(args));
  args.GetReturnValue().Set(ResponseSummary(ds));
}



//private

Local<Object> Sync::ResponseSummary(DeviceStore* ds){

	Isolate *isolate = Isolate::GetCurrent();
    Local<Context> context = isolate->GetCurrentContext();

	Local<Object> result = Object::New(isolate);
	result->Set(context, String::NewFromUtf8(isolate, "added", NewStringType::kNormal).ToLocalChecked(), 	DevicesToV8Array(ds->GetChanges(CHG_ADDED),   DDT_CONNECTION)).Check();
	result->Set(context, String::NewFromUtf8(isolate, "updated", NewStringType::kNormal).ToLocalChecked(), DevicesToV8Array(ds->GetChanges(CHG_UPDATED), DDT_CONNECTION)).Check();
	result->Set(context, String::NewFromUtf8(isolate, "removed", NewStringType::kNormal).ToLocalChecked(), DevicesToV8Array(ds->GetChanges(CHG_REMOVED), DDT_CONNECTION)).Check();

	return result;
}



