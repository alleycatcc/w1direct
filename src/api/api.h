#ifndef API_H
#define API_H

#include "../controller/controller.h"
#include "../shared/v8_helper.h"
#include <node.h>
#include <vector>
#include <string>

#define DEVICE_VECTOR std::vector<Device*>

#define DP_MASTER_NAME "masterName"
#define DP_BUS_NUMBER  "busNumber"
#define DP_DEVICE_ID   "deviceId"
#define DP_DEVICE_IDS  "deviceIds"


using namespace v8;


class Api : public V8Helper {

protected:
   static bool  	  	 AssertParamsFormat(const FunctionCallbackInfo<Value>&);
   static bool  	  	 AssertParam(const FunctionCallbackInfo<Value>&, const char*, const char*);
   static bool  	  	 AssertParamIn(const FunctionCallbackInfo<Value>&, const char*, const char*);
   static bool  	  	 AssertArrayParamIn(const FunctionCallbackInfo<Value>&, const char*, const char*);
   static bool 		  	 AssertDefaultParam(const FunctionCallbackInfo<Value>&, const char*);

   static bool 		  	 AssertMaster(const FunctionCallbackInfo<Value>&);
   static bool 		  	 AssertBus(const FunctionCallbackInfo<Value>&);
   static bool 		  	 AssertDevice(const FunctionCallbackInfo<Value>&);
   static bool 		  	 AssertDevice(const FunctionCallbackInfo<Value>&, std::string*);
   static bool 		  	 AssertDevices(const FunctionCallbackInfo<Value>&);

   static Controller* 	 GetController(const FunctionCallbackInfo<Value>&);
   static Master*     	 GetMaster(const FunctionCallbackInfo<Value>&);
   static Bus*        	 GetBus(const FunctionCallbackInfo<Value>&);
   static Device* 		 GetDevice(const FunctionCallbackInfo<Value>&);
   static DEVICE_VECTOR  GetDevices(const FunctionCallbackInfo<Value>&);

   static std::string 	 GetStrParam(const FunctionCallbackInfo<Value>&, const char*);
   static int 		  	 GetIntParam(const FunctionCallbackInfo<Value>&, const char*);
   static Local<Array>	 GetV8ArrayParam(const FunctionCallbackInfo<Value>&s, const char*);

   static Local<Array>  DevicesToV8Array(std::vector<Device*>*,  int);
   static Local<Object> DevicesToV8Object(std::vector<Device*>*, int);

};


#endif
