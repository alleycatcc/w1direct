#ifndef SYNC_H
#define SYNC_H

#include "api.h"
#include "../controller/controller.h"
#include <node.h>

using namespace v8;


class Sync : public Api {

public:
  static void AllDevices(const FunctionCallbackInfo<Value>&);
  static void MasterDevices(const FunctionCallbackInfo<Value>&);
  static void BusDevices(const FunctionCallbackInfo<Value>&);


private:
  static Local<Object> ResponseSummary(DeviceStore* ds);
  static std::string MasterName(Local<Value>);
  static int BusNumber(Local<Value>);

};


#endif
