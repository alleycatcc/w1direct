#ifndef REGISTER_H
#define REGISTER_H

#include "api.h"
#include <node.h>

using namespace v8;


class Register : public Api   {

public:
  static void DS2482Master(const FunctionCallbackInfo<Value>&);


private:
  static bool DS2482MasterAssertParams(const FunctionCallbackInfo<Value>&);
  static void DS2482MasterSetup(const FunctionCallbackInfo<Value>&);

};


#endif
