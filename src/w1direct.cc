#include <node.h>
#include "manager.h"

using namespace v8;

void InitAll(Local<Object> exports) {
  Manager::Init(exports);
}

NODE_MODULE(w1direct, InitAll)
