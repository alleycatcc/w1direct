#include "manager.h"
#include "controller/controller.h"
#include "api/broadcast.h"
#include "api/read.h"
#include "api/register.h"
#include "api/sync.h"
#include "api/update.h"

#include <string>

using namespace v8;

Persistent<Function> Manager::constructor;

void Manager::Init(Local<Object> exports) {

  Isolate *isolate = exports->GetIsolate();
  Local<Context> context = isolate->GetCurrentContext();

  Local<FunctionTemplate> tpl = FunctionTemplate::New(isolate, New);

  Local<String> identifier = String::NewFromUtf8(isolate, "Manager", NewStringType::kNormal).ToLocalChecked();
  tpl->SetClassName(identifier);
  tpl->InstanceTemplate()->SetInternalFieldCount(1);

  NODE_SET_PROTOTYPE_METHOD(tpl, "broadcastBusCommand", 	Broadcast::BusCommand);
  NODE_SET_PROTOTYPE_METHOD(tpl, "readDevicesById",	 	Read::DevicesById);
  NODE_SET_PROTOTYPE_METHOD(tpl, "registerDS2482Master", Register::DS2482Master);
  NODE_SET_PROTOTYPE_METHOD(tpl, "syncAllDevices", 		Sync::AllDevices);
  NODE_SET_PROTOTYPE_METHOD(tpl, "syncMasterDevices", 	Sync::MasterDevices);
  NODE_SET_PROTOTYPE_METHOD(tpl, "syncBusDevices",	 	Sync::BusDevices);
  NODE_SET_PROTOTYPE_METHOD(tpl, "updateDeviceById",	 	Update::DeviceById);

  Local<Function> fn = tpl->GetFunction(context).ToLocalChecked();
  constructor.Reset(isolate, fn);

  exports->Set(context, identifier, fn).Check();
}



void Manager::New(const FunctionCallbackInfo<Value>& args) {
  Manager* manager = new Manager();
  manager->controller = new Controller();
  manager->Wrap(args.This());
  args.GetReturnValue().Set(args.This());
}
