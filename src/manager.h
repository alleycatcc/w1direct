#ifndef MANAGER_H
#define MANAGER_H

#include <node.h>

#include <node_object_wrap.h>

#include "controller/controller.h"


class Manager : public node::ObjectWrap {
 public:
  static void Init(v8::Local<v8::Object> exports);
  Controller *controller;

 private:
  static void New(const v8::FunctionCallbackInfo<v8::Value>& args);

  static v8::Persistent<v8::Function> constructor;
};



#endif
