#include "v8_helper.h"
#include <string>
#include <stdarg.h>

using namespace v8;
#define MAX_OBJECT_PAIR_STRING 32



std::string V8Helper::GetStdStringFromV8Object(Local<Value> object, const char* key){
	return V8ValueToStdString(GetV8ValueFromV8Object(object, key));
}


int V8Helper::GetIntFromV8Object(Local<Value> object, const char* key){
	return V8ValueToInt(GetV8ValueFromV8Object(object, key));
}


Local<Value> V8Helper::GetV8ValueFromV8Object(Local<Value> object, const char* key){
	Isolate *isolate = Isolate::GetCurrent();
	Local<Context> context = isolate->GetCurrentContext();
	return object->ToObject(context).ToLocalChecked()->Get(
		context,
		String::NewFromUtf8(isolate, key, NewStringType::kNormal).ToLocalChecked()
	).ToLocalChecked();
}


Local<Array> V8Helper::GetV8ArrayFromV8Object(Local<Value> object, const char* key){
	return 	Local<Array>::Cast(GetV8ValueFromV8Object(object, key));
}


std::string V8Helper::V8ValueToStdString(Local<Value> value){
	Isolate *isolate = Isolate::GetCurrent();
	String::Utf8Value str(isolate, value);
	return *str;
}


int V8Helper::V8ValueToInt(Local<Value> value){
	Local<Int32> intValue = value->ToInt32(Isolate::GetCurrent()->GetCurrentContext()).FromMaybe(Local<Int32>());

	return intValue->Value();
}


void V8Helper::AddPairToV8Object(Local<Value> object, const char* key, std::string* value){
	Isolate *isolate = Isolate::GetCurrent();
	Local<Context> context = isolate->GetCurrentContext();
	object->ToObject(context).ToLocalChecked()->Set(
        context,
		String::NewFromUtf8(isolate, key, NewStringType::kNormal).ToLocalChecked(),
		String::NewFromUtf8(isolate, value->c_str(), NewStringType::kNormal).ToLocalChecked()
	).Check();
}


void V8Helper::AddPairToV8Object(Local<Value> object, const char* key, const char* format, ...){
	char msg[MAX_OBJECT_PAIR_STRING+1];
	va_list args;
	va_start(args, format);
	vsnprintf(msg, MAX_OBJECT_PAIR_STRING, format, args);
	va_end(args);

	Isolate *isolate = Isolate::GetCurrent();
	Local<Context> context = isolate->GetCurrentContext();
	object->ToObject(context).ToLocalChecked()->Set(
        context,
		String::NewFromUtf8(isolate, key, NewStringType::kNormal).ToLocalChecked(),
		String::NewFromUtf8(isolate, msg, NewStringType::kNormal).ToLocalChecked()
	).Check();
}


void V8Helper::AddPairToV8Object(Local<Value> object, const char* key, int value){
	Isolate *isolate = Isolate::GetCurrent();
	Local<Context> context = isolate->GetCurrentContext();
	// --- note: cast int to double.
	object->ToObject(context).ToLocalChecked()->Set(
        context,
		String::NewFromUtf8(isolate, key, NewStringType::kNormal).ToLocalChecked(),
		Number::New(isolate, (double)value)
	).Check();
}


void V8Helper::AddPairToV8Object(Local<Value> object, const char* key, bool value){
	Isolate *isolate = Isolate::GetCurrent();
	Local<Context> context = isolate->GetCurrentContext();
	object->ToObject(context).ToLocalChecked()->Set(
        context,
		String::NewFromUtf8(isolate, key, NewStringType::kNormal).ToLocalChecked(),
		Boolean::New(isolate, value)
	).Check();
}


void V8Helper::AddPairToV8Object(Local<Value> object, const char* key, Local<Value> value){
	Isolate *isolate = Isolate::GetCurrent();
	Local<Context> context = isolate->GetCurrentContext();
	object->ToObject(context).ToLocalChecked()->Set(
        context,
		String::NewFromUtf8(isolate, key, NewStringType::kNormal).ToLocalChecked(),
		value
	).Check();
}


bool V8Helper::V8ObjectHasKey(Local<Value> object, const char* key){
	return (GetStdStringFromV8Object(object, key).compare("undefined") != 0);
}


bool V8Helper::V8ValueIsFromDataType(Local<Value> value, const char* key){

	return
	  (value->IsObject() && (key == (const char*) DT_OBJECT)) ||
	  (value->IsArray()  && (key == (const char*) DT_ARRAY))  ||
	  (value->IsString() && (key == (const char*) DT_STRING)) ||
	  (value->IsNumber() && (key == (const char*) DT_NUMBER));

}
